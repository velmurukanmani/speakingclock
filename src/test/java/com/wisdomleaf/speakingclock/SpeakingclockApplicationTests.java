package com.wisdomleaf.speakingclock;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.wisdomleaf.speakingclock.service.IClockService;

@SpringBootTest
class SpeakingclockApplicationTests {
	
	@Autowired
	private IClockService iClockService;
	
	String validTime = "12:00";
	String invalidTime = "12:01";
		
	@Test
	void successTest() {
		assertEquals("It's Midday", iClockService.convertTimeToText(validTime));
	}
	
	@Test
	void failureTest() {
		assertEquals("It's Midday", iClockService.convertTimeToText(invalidTime));
	}

}
