package com.wisdomleaf.speakingclock.util;

public class ClockUtils {
	
	public static String convert(final int n) {
		
        final String[] units = { "", "one", "two", "three", "four",
                "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve",
                "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
                "eighteen", "nineteen" };

        final String[] tens = {
                "", 		// 0
                "",		// 1
                "twenty", 	// 2
                "thirty", 	// 3
                "forty", 	// 4
                "fifty", 	// 5
                "sixty", 	// 6
                "seventy",	// 7
                "eighty", 	// 8
                "ninety" 	// 9
        };

        String stringValue = "";

        if (n < 20) {
            stringValue = units[n];
        }

        else if (n < 100) {
            stringValue = tens[n / 10] + ((n % 10 != 0) ? " " : "") + units[n % 10];
        }
        
        return stringValue;
    }
}
