package com.wisdomleaf.speakingclock.service.impl;

import org.springframework.stereotype.Service;

import com.wisdomleaf.speakingclock.service.IClockService;
import com.wisdomleaf.speakingclock.util.ClockUtils;

@Service
public class ClockService implements IClockService {

	@Override
	public String convertTimeToText(String time) {
		
		String result = null;
		
		try {
			if (time != null) {
				String[] strs = time.trim().split(":");

		        int hours = Integer.parseInt(strs[0]);
		        int mins = Integer.parseInt(strs[1]);

				if (hours == 12 && mins == 0) {
					result = "It's Midday";
				} 
				
				else if (hours == 0 && mins == 0) {
		        	result = "It's Midnight";
		        } 
				
				else {
		        	result = "It's " + ClockUtils.convert(hours) + " ";

		            if (mins > 9) {
		            	result = result +  ClockUtils.convert(mins) + " ";
		            }
		            
		            else if (mins < 9 && mins > 0){
		            	result = result + " oh " + ClockUtils.convert(mins) + " ";
		            }
		        } 
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception is :: " + e.getLocalizedMessage());
		}
		
		return result;
	}
	
}
