package com.wisdomleaf.speakingclock.service;

public interface IClockService {
	
	public String convertTimeToText(String time);
}
