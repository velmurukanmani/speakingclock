package com.wisdomleaf.speakingclock.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wisdomleaf.speakingclock.service.IClockService;

@RestController
@RequestMapping("/api")
public class ClockController {
	
	@Autowired
	public IClockService iClockService;
	
	@GetMapping
	public String index() {
		
		String time = "00:00";
		return iClockService.convertTimeToText(time);
	}

}
